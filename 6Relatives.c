#include <stdio.h>
#include <string.h>
#define N 80
#define M 10

int main()
{
	char str[M][N];
	int num=0,i=0,age=0,ageOld=0,ageYoung=200;
	char *young=str,*old=str;
	printf("Please enter a number of relatives\n");
	scanf("%d", &num);
	for (i=0;i<num;i++){
		fflush(stdin);
		puts("Please enter a name");
		fgets(str[i],N,stdin);
		puts("Please enter an age of current relative");
		scanf("%d", &age);
		if (age<ageYoung){
			ageYoung=age;
			young = str[i];
		}
		if (age>ageOld){
			ageOld=age;
			old = str[i];
		}
	}
	printf("The youngest relative is %s\n", young);
	printf("The oldest relative is %s\n", old);
	return 0;
}