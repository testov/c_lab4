#include <stdio.h>
#define N 6

int main()
{
	char str[N];
	char *p=str, *p2=&str[N-2];
	puts("Please enter a string with 5 elements");
	fgets(str, N, stdin);
	
	while(*p){
		if(*p!=*p2){
			puts("It is not a palindrome");
			break;
		}
		p++;
		p2--;
	}
	if(!*p){
		puts("Congratulations! It is palindrome!");
	}
	return 0;
}