#include <stdio.h>
#include <string.h>
#define N 80
#define M 10

int main()
{
	char str[M][N];
	char *pstr[M], *buff;
	int i=0, k, l, len1, len2;

	FILE *file;
 
	file = fopen("example.txt", "r");
 	if(file==0)
		return 1;
	while (fgets (str[i], N, file) != NULL){
		pstr[i]=&str[i][0];
		i++;
	}
	for(k=0;k<i;k++){
		for(l=i-1;l>k;l--){
			len1 = strlen(pstr[l]);
			len2 = strlen(pstr[l-1]);
			if(len1<len2){
				buff = pstr[l-1];
				pstr[l-1] = pstr[l];
				pstr[l] = buff;
			}				
		}
	}
	for(k=0;k<i;k++)
		puts(pstr[k]);
	return 0;
}
