#include <stdio.h>
#define N 80

int main()
{
	char str[N];
	char *pstr[N];
	int k=0, i=0, l;
	puts("Please enter a string");
	fgets(str,N,stdin);
	if (str[0]!=' '){
		pstr[k]=str;
		k++;
	}
	while(str[i]){
		if(str[i-1]==' ' && str[i]!=' '){
			pstr[k]=&str[i];
			k++;
		}
		i++;
	}
	for(l=k-1;l>=0;l--){
		while(*pstr[l]!='\n' && *pstr[l]!=' '){
			putchar(*pstr[l]);
			pstr[l]++;
		}
		putchar(' ');
	}
	return 0;
}